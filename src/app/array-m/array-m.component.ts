import { Component } from '@angular/core';
interface IStudent {
  Id: number;
  FirstName: string;
  LastName: string;
  Email:string;
  ContactNo:string;
  
}
interface IData  
  {
    Id:number;
    FirstName:String;
    LastName: String;
    Email: String;
    ContactNo:string;
  }


@Component({
  selector: 'app-array-m',
  templateUrl: './array-m.component.html',
  styleUrls: ['./array-m.component.scss']
})
export class ArrayMComponent {

  constructor(){
    this.cons();
    this.adddata();
    this.ata();
    console.log(this.megha);
    this.last();
    this.arr();
    this.splice();
    this.array();
    this.remove();
    this.ani();
    this.copy();
    this.sort();
    this.map();
  }
  //1 1D array of string values, and add the element to the array using concat() function
  myArray: string[] = ['apple', 'banana', 'cherry'];
  cons(){
  
    this.myArray= this.myArray.concat('dates');
  }
   // 2:2D array named DataSet, Consider the object keys such as Id, FirstName, LastName, Email, ContactNo Sample 2D Array
  // Replace any type definition with an actual type definition through an interface
  DataSet0:IStudent[]= [
    
    {Id: 1,  FirstName: 'Megha',LastName: 'Talekar', Email: 'megha@gmail.com',ContactNo:'666-666-666'},
    {Id: 2,  FirstName: 'Meghana',LastName: 'Janardhan',Email: 'meghana@gmail.com',ContactNo:'777-777-777'},
    {Id: 3,  FirstName: 'Anjali',LastName: 'Jadhav', Email: 'anjali@gmail.com',ContactNo:'888-888-888'}
  
];
  //3:add sample objects using push() function 
  public DataSet: IData[] =  [
    {
      Id: 1,
      FirstName: "John",
      LastName: "Doe",
      Email: "johndoe@example.com",
      ContactNo: "555-555-5555"
    },
    {
      Id: 2,
      FirstName: "Jane",
      LastName: "Doe",
      Email: "janedoe@example.com",
      ContactNo: "555-555-5555"
    },
    {
      Id: 3,
      FirstName: "Bob",
      LastName: "Smith",
      Email: "bobsmith@example.com",
      ContactNo: "555-555-5555"
    }
  ]
  //3:add sample objects using push() function 
  adddata(){
      let newdata: IData={
      Id: 4,
      FirstName: "Megha",
      LastName: "Talekar",
      Email: "megha@example.com",
      ContactNo: "555-555-5555"
  
  }
  this.DataSet.push(newdata);
}
// 4:To return the values of push() function
megha=this.DataSet.push();

//5:The push() function to add elements to the beginning of an array
//Ans:push function cannot be used to add element at beginning of the array
//6:How do you use the push() function to add an element to an array only if it doesn't already exist in the array
ata(){
  let data: IData={
  Id: 5,
  FirstName: "Bhagya",
  LastName: "Talekar",
  Email: "bhagya@example.com",
  ContactNo: "999-999-999"

}
if(!this.DataSet.includes(data)) {
  this.DataSet.push(data);
}
}
//7 use the push() function with the spread operator to concatenate two arrays
array1: number[] = [1, 2, 3];
array2: number[] = [4, 5, 6];
newarray:number[]=[...this.array1, ...this.array2];

//8 use the pop() function to remove the last element from an array
last(){
  let lastElement=this.newarray.pop();
}
//9 The return value of the pop() function is the element that was removed

//10  check if an array is empty using the pop() function
array3: number[] = [];
arr(){
  if (this.array3.pop() === undefined) {
    console.log('The array is empty');
  } else {
    console.log('The array is not empty');
  }
  //11  the pop() function is used to remove the last element from an array
  //12  pop() function in combination with the push() function to implement a stack data structure
  class Stack {
    private items: any[] = [];
  
    push(item: any): void {
      this.items.push(item);
    }
  
    pop(): any {
      return this.items.pop();
    }
  
    isEmpty(): boolean {
      return this.items.length === 0;
    }
  
  }
  
  let myStack = new Stack();
  myStack.push(1);
  myStack.push(2);
  myStack.push(3);
  
  console.log(myStack.pop()); // Output: 3
  console.log(myStack.pop()); // Output: 2
  console.log(myStack.pop()); // Output: 1
  console.log(myStack.isEmpty()); // Output: true
  
}

// 13 splice() function to remove elements from an array at a specific index
  sp = [1, 2, 3, 4, 5];
splice(){

  // Remove the element at index 2
  this.sp.splice(2, 1);
}
//14 the splice() function to remove a range of elements from an array
range = ["cars", "bikes", "cycles", "buses", "train"];
array(){
  // Remove elements from index 2 to index 4
this.range.splice(2, 3);
}
//15 the return value of the splice() function
fruits = ['apple', 'banana', 'orange', 'grape'];
remove(){
  const removed = this.fruits.splice(1, 2);
console.log(removed); 
console.log(this.fruits); 
}
//16 splice() function  used to replace elements in an array
animals = ['amphibians', 'animals', 'birds'];
ani(){
  // Replace the element at index 1 with 'wild_animals'
this.animals.splice(1, 1, 'wild_animals');
console.log(this.animals); 
}
//17  difference between the slice() and splice() functions
// slice() is a method that creates a new array containing a copy of a portion of the original array
// splice() is a method that modifies the original array by removing and/or adding elements. 

//18  the slice() function to copy an entire array
original = ['a', 'b', 'c','d','e'];
copy(){
const copiedArray = this.original.slice();
console.log(this.original)
console.log(copiedArray);
}
//19 sort the 2D array data based upon the id in descending order Hint - Use sort()
public DataSet1: IData[] =  [
  {
    Id: 1,
    FirstName: "John",
    LastName: "Doe",
    Email: "johndoe@example.com",
    ContactNo: "555-555-5555"
  },
  {
    Id: 2,
    FirstName: "Jane",
    LastName: "Doe",
    Email: "janedoe@example.com",
    ContactNo: "555-555-5555"
  },
  {
    Id: 3,
    FirstName: "Bob",
    LastName: "Smith",
    Email: "bobsmith@example.com",
    ContactNo: "555-555-5555"
  }
]
sort(){
 this.DataSet1.sort((a, b) => b.Id - a.Id);
 console.log(this.DataSet1);
}
//20 Return a separate 2D array only having the element IDs of the intial 2D array Hint use map()
map(){
  const Ids = this.DataSet.map(item => [item.Id]);
  console.log(Ids);
}
  
}

