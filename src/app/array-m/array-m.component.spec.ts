import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrayMComponent } from './array-m.component';

describe('ArrayMComponent', () => {
  let component: ArrayMComponent;
  let fixture: ComponentFixture<ArrayMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArrayMComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArrayMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
